extends Node2D

func _ready():
	$SmallAsteroid1.get_component("velocity").direction = Vector2.RIGHT
	$SmallAsteroid2.get_component("velocity").direction = Vector2.RIGHT
	$SmallAsteroid3.get_component("velocity").direction = Vector2.RIGHT
	
#	var shield = Game.entity_shield.instance()
#	$Node2D.add_child(shield)
#	shield.global_position = Vector2(240, 240)

	$OrbitController.create_orbit()
	
func _physics_process(delta):
	ECS.update("physics")
	
func _process(delta):
	ECS.update("systems")
