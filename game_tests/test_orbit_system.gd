extends Node2D

export var AUTO_ORBIT : bool = true

onready var shields = $ShieldOrbitController
onready var satellites = $SatelliteOrbitController

func _ready():
	
	Game.shield_orbiter = shields
	
	# build orbit scenes
	shields.create_orbit()
	satellites.create_orbit()
	
	if (!AUTO_ORBIT):
		return
		
	# move satellites to proper position
	var _node = 0
	
	for satellite in satellites.scene_nodes:
		satellite.global_position = satellites.position_nodes[_node].global_position
		_node += 1

	# move shields  to proper position
	_node = 0
	
	for shield in shields.scene_nodes:
		shield.global_position = shields.position_nodes[_node].global_position
		_node += 1
