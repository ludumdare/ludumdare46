extends Node2D

onready var message = get_node("Label")


func _ready():
	DreamLo.connect("dreamlo_added_score", self, "_on_added_score")
	DreamLo.connect("dreamlo_got_scores", self, "_on_got_scores")
	DreamLo.connect("dreamlo_adding_score", self, "_on_adding_score")
	DreamLo.connect("dreamlo_getting_scores", self, "_on_getting_scores")
	DreamLo.connect("dreamlo_clearing_scores", self, "_on_clearing_scores")
	DreamLo.connect("dreamlo_cleared_scores", self, "_on_cleared_scores")
	

func _on_Clear_pressed():
	DreamLo.clear_scores()


func _on_Add_pressed():
	var players = ['Joe', 'Johnny', 'Mark', 'Suzie', 'Paul', 'Jeremy', 'Martin', 'sP0CkEr2']
	var score = int(rand_range(100, 1000))
	DreamLo.add_score(players[rand_range(0,players.size())], str(score))


func _on_Show_pressed():
	DreamLo.get_scores()


func _on_adding_score():
	message.text = "Adding Score"

	
func _on_added_score():
	message.text = "Score Added"
	
	
func _on_clearing_scores():
	message.text = "Clearing Scores"

	
func _on_cleared_scores():
	message.text = "Scores Cleared"
	

func _on_getting_scores():
	message.text = "Getting Scores"
	
	
func _on_got_scores(scores):
	
	message.text = "Got Scores"
	var data = get_node("Panel/Data")
	
	data.text = ""
	
	for s in scores:
		data.text += "{name} : {score}\n".format({ "name": s.name, "score": s.score })
