extends Node2D

const entity_orbit_position = preload("res://game/entities/orbit_node.tscn")
const entity_shield = preload("res://game/entities/shield.tscn")

const entity_small_asteroid = preload("res://game/entities/small_asteroid.tscn")

const particle_shield_hit = preload("res://game/particles/shield_hit.tscn")
const particle_planet_hit = preload("res://game/particles/planet_hit.tscn")
const particle_asteroid_hit = preload("res://game/particles/asteroid_hit.tscn")

const sound_zap = preload("res://game/audio/electrical_zap_rnd_04.wav")
const sound_energy = preload("res://game/audio/energy_field_lp_01.wav")
const sound_explode = preload("res://game/audio/explosion_distant_rnd_03.wav")
const sound_twinkle = preload("res://game/audio/outer_space_twinkle_lp_01.wav")
const sound_noise = preload("res://game/audio/short_noise_zap_rnd_01.wav")
const sound_background = preload("res://game/audio/strange_phasey_background_lp_01.wav")
const sound_world = preload("res://game/audio/alien_world_lp_01.wav")
const sound_whoosh = preload("res://game/audio/medium_basic_whoosh_rnd_01.wav")

signal spawn(key, direction)
signal wave_item_destroyed(key)
signal wave_completed()
signal wave_damage(value)
signal add_score(value)
signal add_currency(value)

var background1
var background2


enum GameState {
	GAME_STATE_NONE,
	GAME_STATE_INIT,
	GAME_STATE_TITLE,
	GAME_STATE_ABOUT,
	GAME_STATE_PLAY,
	GAME_STATE_LOSE,
	GAME_STATE_WIN,
	GAME_STATE_JOB,
	GAME_STATE_SHOP,
	GAME_STATE_IDLE
}

var shield_orbiter
var game_state
var next_state
var travel_state

var data : GameData

var current_player : PlayerData

var curtain = 1.0

func change_state(state):
	Logger.trace("[Game] change_state")
	next_state = state
	

func play_zap():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = sound_zap
	player.play()
	
	
func play_energy():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = sound_energy
	player.play()
	

func play_explode():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = sound_explode
	player.play()
	

func play_whoosh():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = sound_whoosh
	player.play()
	

func _process(delta):
	
	match game_state:
		
		GameState.GAME_STATE_INIT:
			_game_state_init()
			
		GameState.GAME_STATE_TITLE:
			_game_state_title()
			
		GameState.GAME_STATE_ABOUT:
			_game_state_about()
			
		GameState.GAME_STATE_LOSE:
			_game_state_lose()
			
		GameState.GAME_STATE_WIN:
			_game_state_win()
			
		GameState.GAME_STATE_JOB:
			_game_state_job()
			
		GameState.GAME_STATE_SHOP:
			_game_state_shop()
			
		GameState.GAME_STATE_PLAY:
			_game_state_play()
			
			
	game_state = next_state
			
func _game_state_init():
	Logger.trace("[Game] _game_state_init")
	change_state(GameState.GAME_STATE_TITLE)

	
func _game_state_title():
	Logger.trace("[Game] _game_state_home")
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_TITLE
	SceneManager.transition_to("res://game/scenes/travel.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)
	
	
func _game_state_play():
	Logger.trace("[Game] _game_state_play")
	SceneManager.transition_to("res://game/scenes/play.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)


func _game_state_lose():
	Logger.trace("[Game] _game_state_lose")
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_LOSE
	SceneManager.transition_to("res://game/scenes/travel.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)

func _game_state_win():
	Logger.trace("[Game] _game_state_lose")
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_WIN
	SceneManager.transition_to("res://game/scenes/travel.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)

func _game_state_job():
	Logger.trace("[Game] _game_state_job")
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_JOB
	SceneManager.transition_to("res://game/scenes/travel.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)

func _game_state_shop():
	Logger.trace("[Game] _game_state_shop")
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_SHOP
	SceneManager.transition_to("res://game/scenes/travel.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)

func _game_state_about():
	Logger.trace("[Game] _game_state_about")
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_ABOUT
	SceneManager.transition_to("res://game/scenes/travel.tscn", curtain )
	change_state(GameState.GAME_STATE_IDLE)


func _ready():
	Logger.trace("[Game] _ready")
	change_state(GameState.GAME_STATE_NONE)
	current_player = PlayerData.new()
	background1  = AudioStreamPlayer.new()
	self.add_child(background1)	
	background1.stream = sound_background
	background1.play()
	background1.volume_db = -10
	background2  = AudioStreamPlayer.new()
	self.add_child(background2)	
	background2.stream = sound_world
	background2.play()
	background2.volume_db = -10
	
func _get_monster(data : String):
	var _monster : MonsterData = MonsterData.new()
	var _parms = data.split(";")
	_monster.frequency = _get_range(_parms[0])
	_monster.strength = _get_range(_parms[1])	
	return _monster
	
	
func _get_range(data : String):
	var _values = data.split(":")
	var _range = RangeData.new()
	_range.min_value = _values[0]
	_range.max_value = _values[1]
	return _range
	
	
func _load_shops(config : ConfigFile):
	var _shops = []
	for key in config.get_section_keys("shop"):
		var _shop = ShopData.new()
		var _data : String = config.get_value("shop", key)
		var _parms = _data.split(";")
		_shop.name = _parms[0]
		_shop.cost = _parms[1]
		_shop.limit = _parms[2]
		_shops.append(_shop)		
		
	return _shops
	
	
func _load_upgrades(config: ConfigFile):
	var _upgrades = []
	for key in config.get_section_keys("upgrade"):
		var _upgrade = UpgradeData.new()
		var _data : String = config.get_value("upgrade", key)
		var _parms = _data.split(";")
		_upgrade.name = _parms[0]
		_upgrade.cost = _parms[1]
		_upgrade.limit = _parms[2]
		_upgrades.append(_upgrade)		
		
	return _upgrades
	
	
func _load_jobs(config_file : ConfigFile):
	var _jobs = []
	for job_number in range(4):
		
		var job = JobData.new()
		var _job = "job{0}".format([job_number])
		job.name = config_file.get_value(_job, "name")
		job.rank = 	config_file.get_value(_job, "rank")
		job.value = config_file.get_value(_job, "value")
		job.asteroid0 = _get_range(config_file.get_value(_job, "asteroid0"))
		job.asteroid1 = _get_range(config_file.get_value(_job, "asteroid1"))
		job.asteroid2 = _get_range(config_file.get_value(_job, "asteroid2"))
		job.asteroid3 = _get_range(config_file.get_value(_job, "asteroid3"))
		job.alien0 = _get_range(config_file.get_value(_job, "alien0"))
		job.alien1 = _get_range(config_file.get_value(_job, "alien1"))
		job.monster = _get_monster(config_file.get_value(_job, "monster0"))
		job.strength = _get_range(config_file.get_value(_job, "strength"))
		job.fine = config_file.get_value(_job, "fine")
		_jobs.append(job)
		
	return _jobs


func _load_data(config_file : ConfigFile):
	data.name = config_file.get_value("game", "name")
	data.version = config_file.get_value("game", "version")
	data.copyright = config_file.get_value("game", "copyright")
	data.ludumdare = config_file.get_value("game", "ludumdare")
	data.message = config_file.get_value("game", "message")
	data.jobs = _load_jobs(config_file)
	data.shops = _load_shops(config_file)
	data.upgrades = _load_upgrades(config_file)
	
func _init():
	
	randomize()
	
	Logger.trace("[Game] _init")
	
	travel_state = TravelScene.TravelStates.TRAVEL_STATE_NONE
	
	data = GameData.new()
	current_player = PlayerData.new()

	var config = Configuration.new()
	var config_file = config.load_config("res://game.cfg") as ConfigFile
	
	if (config_file):
		_load_data(config_file)
		
	print()
	print(data.name)
	print(data.version)
	print(data.copyright)
	print()
	print("found {0} jobs".format([data.jobs.size()]))
	print("found {0} shops".format([data.shops.size()]))
	print("found {0} upgrades".format([data.upgrades.size()]))
	print()
	
