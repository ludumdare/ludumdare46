extends Node2D

var timer

func _ready():
	$Particles2D.one_shot = true
	timer = Timer.new()
	add_child(timer)
	timer.wait_time = 2.0
	timer.connect("timeout", self, "_on_timeout")
	timer.start()
	
func _on_timeout():
	queue_free()
