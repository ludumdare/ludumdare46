extends Entity

func _ready():
	pass


func _on_Timer_timeout():
	ECS.remove_entity(self)
	Game.emit_signal("wave_item_destroyed", name)
