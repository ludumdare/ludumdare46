class_name PlayerController
extends Node

export(NodePath) var ORBIT_CONTROLLER
export var INCREASE : float = 7.0
export var MAX : float = 35.0

var rotation_direction = 0
var orbiter : Entity

func _input(event):
	
	if event is InputEventMouseButton:
			if event.is_pressed():
				if event.button_index == 5: # Scroll Down
					if (rotation_direction < 0):
						rotation_direction = 0
						
					rotation_direction += INCREASE
	 
				if event.button_index == 4: # Scroll Up
					if (rotation_direction >0):
						rotation_direction = 0
				
					rotation_direction -= INCREASE
	 
				if event.button_index == 3: # middle
					rotation_direction = 0
					
	
func _process(delta):
	
	var _rotates = orbiter.get_component("rotates") as Rotates
	
	if (!_rotates):
		return
	
	if (Input.is_action_pressed("game_up")):
		
		if (rotation_direction >0):
			rotation_direction = -INCREASE
			
		rotation_direction -= (INCREASE * .45)
		
	if (Input.is_action_pressed("game_down")):
		
		if (rotation_direction < 0):
			rotation_direction = INCREASE
			
		rotation_direction += (INCREASE * .45)		
	
	if (Input.is_action_pressed("game_left")):
		
		if (rotation_direction >0):
			rotation_direction = -INCREASE
			
		rotation_direction -= (INCREASE * .20)
		
	if (Input.is_action_pressed("game_right")):
		
		if (rotation_direction < 0):
			rotation_direction = INCREASE
			
		rotation_direction += (INCREASE * .20)		
	
	if (rotation_direction > MAX):
		rotation_direction = MAX
		
	if (rotation_direction < -MAX):
		rotation_direction = -MAX
		
#	if (rotation_direction < 0):
#		zero = -1.0
#
#	if (rotation_direction >= 0):
#		zero = 1.0
#
#	print(rotation_direction)
		
	_rotates.ROTATION_DIRECTION = rotation_direction
	rotation_direction = lerp(rotation_direction, 0.0, delta * INCREASE )
	

func _ready():
	orbiter = get_node(ORBIT_CONTROLLER) as Entity
