"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: OrbitPosition
	
		Defines a Position for something that Orbits
	
	Remarks:

		In general we will use these nodes around the ShieldOrbit
		entity to give orbiting entities something to move towards.
		
		This will be easier than calculating the math every single time
		we want to move something back into orbit.
			
"""

class_name OrbitPosition
extends Entity

func _ready():
	pass
