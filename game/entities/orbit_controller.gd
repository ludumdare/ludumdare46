"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: OrbitController
	
		Controls the Orbitting around an Entity
	
	Remarks:
		
		This entity is responsible for creating the slots
		used by each shield entity that orbits around
		the entity being defended.
		
		We use some math here to calculate the distance from
		each other evenly around the entity.
		
		This entity will also add the Shield nodes and attach
		them to a orbit slot.
"""

class_name OrbitController
extends Entity

export var ORBIT_RADIUS = 100
export var ORBIT_NODES = 10
export(PackedScene) var ORBIT_SCENE

var orbit_positions = []
var position_nodes = []
var scene_nodes = []

func create_orbit():
	Logger.trace("[orbit_controller] create_orbit")
	_create_orbit_positions()
	_create_position_nodes()
	_create_scene_nodes(self)
	
# calculate and store the orbit positions from a center point
func _create_orbit_positions():
	
	Logger.trace("[orbit_controller] _create_orbit_positions")
	
	var _theta = 2.0 * PI / ORBIT_NODES
	
	orbit_positions.clear()
	
	for i in range(ORBIT_NODES):
		var _point_x : float = (ORBIT_RADIUS * cos(_theta * i))
		var _point_y : float = (ORBIT_RADIUS * sin(_theta * i))
		orbit_positions.append(Vector2(_point_x, _point_y))
		
		
func _create_position_nodes():
	
	Logger.trace("[orbit_controller] _create_position_nodes")
	
	for position in orbit_positions:
		var eop = Game.entity_orbit_position.instance()
		add_child(eop)
		eop.position = position
		var _orbits = eop.get_component("orbits") as Orbits
		_orbits.orbit_entity = self
		position_nodes.append(eop)
	
func _create_scene_nodes(entity: Entity):
	
	Logger.trace("[orbit_controller] _create_scene_nodes")
	
	for orbit_node in position_nodes:
		var _scene_node = ORBIT_SCENE.instance()
		get_parent().add_child(_scene_node)
		_scene_node.position = Vector2.ZERO
		scene_nodes.append(_scene_node)
		
		if (_scene_node.has_component("moveto")):
			var _moveto = _scene_node.get_component("moveto") as MoveTo
			_moveto.target = orbit_node
	
