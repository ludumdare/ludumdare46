extends Entity

onready var timer = $Timer
var spawner : Spawner

func _ready():
	
	spawner = get_component("spawner")
	timer.wait_time = spawner.SPAWN_DELAY + rand_range(-2, +2)
	timer.start()
	
func _on_Timer_timeout():
	spawner.spawn = true
	timer.wait_time = spawner.SPAWN_DELAY + rand_range(-2, +2)
	timer.start()
