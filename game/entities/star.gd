extends Entity


func _ready():
	get_component("velocity").SPEED_FACTOR = rand_range(0.05, 3.5)

func _on_Timer_timeout():
	ECS.remove_entity(self)
