extends DebugNode

func on_draw() -> void:
	
	var _orbits = entity.get_component("orbits") as Orbits
	var color = DEBUG_COLOR_LINE_6
	
	draw_circle_arc(entity.global_position, 5, 0, 360, color)
	draw_line(_orbits.orbit_entity.global_position, entity.global_position, color)
	
