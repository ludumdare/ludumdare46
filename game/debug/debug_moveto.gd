extends DebugNode

func on_draw() -> void:
	
	var _moveto = entity.get_component("moveto") as MoveTo
	draw_line(_moveto.target.global_position, entity.global_position, DEBUG_COLOR_LINE_1)
	
