class_name PlayerData
extends Resource

var name: String
var currency: int
var rank: int 
var satellites: int
var shields: int 
var rockets: int
var experience: int
var hiscore: int
var score: int
var last_earning : int

func _init():
	
	name = "n00b"
	satellites = 1
	shields = 12
	rockets = 0
	currency = 0
	last_earning = 0
	
