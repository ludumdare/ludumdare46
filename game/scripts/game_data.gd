class_name GameData
extends Resource

var name : String = "None"
var version : String = "0.0.0"
var copyright : String = "Copyright 2020 SpockerDotNet LLC, All Rights Reserved."
var ludumdare : String = "LD"
var message : String = "Hello World"
var jobs = []
var shops = []
var upgrades = []

