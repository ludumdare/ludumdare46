class_name BaseSpawner
extends Node2D

export var SPAWN_DELAY = 0.0
export var SPAWN_WARMUP = 0.0
export(String, "UP", "DOWN", "LEFT", "RIGHT")  var SPAWN_DIRECTION = "LEFT"
export var SPAWN_SPREAD = 0
export var SPAWN_COUNT = -1
export(PackedScene) var SPAWN_SCENE

var count = 0
var active = true
var key = "base0"

func spawn(key: String, direction : String):
	Logger.trace("[BaseSpawner] spawn")
	
	if (name != key):
		return
	
	SPAWN_DIRECTION = direction
	
	on_before_spawn()

	var _spawn = SPAWN_SCENE.instance()
	add_child(_spawn)
	_spawn.position = _get_start_position() + _get_spread()
	_spawn.get_component("velocity").direction = _get_direction()
		
	on_spawn(_spawn)
	
	on_after_spawn(_spawn)
	
	
func on_before_spawn():
	pass

func on_spawn(entity: Entity):
	pass	
	
func on_after_spawn(entity: Entity):
	pass
	

func _get_spread():
	
	if (SPAWN_DIRECTION == "LEFT" or SPAWN_DIRECTION == "RIGHT"):
		return Vector2(0, rand_range(-SPAWN_SPREAD, +SPAWN_SPREAD))
	
	if (SPAWN_DIRECTION == "UP" or SPAWN_DIRECTION == "DOWN"):
		return Vector2(rand_range(-SPAWN_SPREAD, +SPAWN_SPREAD), 0)

	
func _get_start_position():
	
	if (SPAWN_DIRECTION == "LEFT"):
		return Vector2.RIGHT * 300
		
	if (SPAWN_DIRECTION == "RIGHT"):
		return Vector2.LEFT * 300
		
	if (SPAWN_DIRECTION == "UP"):
		return Vector2.DOWN * 300
		
	if (SPAWN_DIRECTION == "DOWN"):
		return Vector2.UP * 300
		

func _get_direction():
	
	if (SPAWN_DIRECTION == "LEFT"):
		return Vector2.LEFT
		
	if (SPAWN_DIRECTION == "RIGHT"):
		return Vector2.RIGHT
		
	if (SPAWN_DIRECTION == "UP"):
		return Vector2.UP
		
	if (SPAWN_DIRECTION == "DOWN"):
		return Vector2.DOWN
	
			
func _ready():
	
	Game.connect("spawn", self, "spawn")
	
	if (SPAWN_DELAY > 0):
		$Timer.wait_time = SPAWN_DELAY
		$Timer.autostart = true
		$Timer.start()


func _on_Timer_timeout():
	
	if (!active):
		return
	
	count += 1
	
	if (SPAWN_COUNT < 0):
		count = 0
	
	if (SPAWN_COUNT > 0 and count > SPAWN_COUNT):
		return
		
	spawn(name, SPAWN_DIRECTION)
		
	$Timer.start()
