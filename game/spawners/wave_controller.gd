class_name WaveController
extends Node2D

export var SPAWN_DELAY = 1.0

var active = true
var wave_items = []
var count = 0

func init(wave: WaveData):
	wave_items = wave.items	
	$Timer.start()
	
func on_before_spawn():
	Logger.trace("[WaveController] on_before_spawn")

func on_spawn(wave_item : WaveItem):
	Logger.trace("[WaveController] on_spawn")
	Game.emit_signal("spawn", wave_item.key, wave_item.direction)
	
func on_after_spawn(wave_item: WaveItem):
	Logger.trace("[WaveController] on_after_spawn")
	

			
func _ready():
	$Timer.wait_time = SPAWN_DELAY
	$Timer.autostart = true


func _on_Timer_timeout():
	
	if (!active):
		return
		
	on_before_spawn()
	
	var _wave_item = wave_items[count]
		
	on_spawn(_wave_item)
	
	on_after_spawn(_wave_item)
	
	count += 1
	
	if (count >= wave_items.size()):
		active = false
		return	
	
	$Timer.start()
