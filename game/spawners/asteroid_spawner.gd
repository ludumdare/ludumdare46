class_name AsteroidSpawner
extends BaseSpawner

func on_spawn(entity: Entity):
	
	var target_range = 60
	
	var _velocity = entity.get_component("velocity") as Velocity
	
	var target = Vector2(240,240)
	target.x += (rand_range(-target_range, target_range))
	target.y += (rand_range(-target_range, target_range))
	
	_velocity.direction = (target - entity.global_position).normalized()
	
