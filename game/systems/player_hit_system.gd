class_name PlayerHitSystem
extends System

func on_process_entity(entity: Entity, delta: float):

	var _collides = entity.get_component("collides") as Collides
	
	if (!_collides.has_collided):
		return
		
	Logger.info("boom goes the player")
	
	Game.play_explode()

	var _damage = _collides.entity.get_component("damage") as Damage

	var p = Game.particle_planet_hit.instance()
	entity.get_parent().add_child(p)
	p.global_position = _collides.entity.global_position
	Logger.info("p: {0}; entity: {1}".format([p.global_position, entity.global_position]))
	Game.emit_signal("wave_damage", _damage.DAMAGE_VALUE)
