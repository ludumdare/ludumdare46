extends System

func on_process_entity(entity: Entity, delta: float):

	var _collides = entity.get_component("collides") as Collides
	
	if (!_collides.has_collided):
		return
		
	Logger.info("boom goes the asteroid")
	entity.remove_component("collides")
	ECS.remove_entity(entity)
	Game.emit_signal("wave_item_destroyed", entity.name)
	
	Game.play_explode()

	var p = Game.particle_asteroid_hit.instance()
	entity.get_parent().add_child(p)
	p.global_position = entity.global_position
	Logger.info("p: {0}; entity: {1}".format([p.global_position, entity.global_position]))
