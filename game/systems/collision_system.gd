
extends System
class_name CollidesSystem

func on_process_entity(entity: Entity, delta: float):
	
	var _collides = entity.get_component("collides") as Collides
	
	if (!_collides.active):
		return

	var o = entity.get_overlapping_areas()

	_collides.has_collided = false
	_collides.entity = null
	
	
	if o.size() > 0:

		Logger.info("- {0} has collided with {1}".format([entity.name, o[0].name]))
		_collides.has_collided = true
		_collides.entity = o[0]
