class_name SpawnSystem
extends System

func on_process_entity(entity: Entity, delta: float):
	
	var _spawner = entity.get_component("spawner") as Spawner
	
	if (!_spawner.spawn):
		return
		
	print("spawn")
	_spawner.spawn = false
		
	var _spawn = _spawner.SPAWN_SCENE.instance()
	
	entity.add_child(_spawn)
	
	# figure out the starting location
	
	var loc
	
	if (_spawner.SPAWN_LOCATION == Spawner.SpawnLocation.SPAWN_LOCATION_TOP):
		loc = Vector2.UP
		
	if (_spawner.SPAWN_LOCATION == Spawner.SpawnLocation.SPAWN_LOCATION_BOTTOM):
		loc = Vector2.DOWN
				
	if (_spawner.SPAWN_LOCATION == Spawner.SpawnLocation.SPAWN_LOCATION_LEFT):
		loc = Vector2.LEFT
				
	if (_spawner.SPAWN_LOCATION == Spawner.SpawnLocation.SPAWN_LOCATION_RIGHT):
		loc = Vector2.RIGHT
				
	var pos = loc * 240
					
	# adjust starting location
	
	if (loc == Vector2.UP or loc == Vector2.DOWN):
		pos.x += rand_range(-_spawner.SPAWN_SPREAD, _spawner.SPAWN_SPREAD)
		
	if (loc == Vector2.RIGHT or loc == Vector2.LEFT):
		pos.y += rand_range(-_spawner.SPAWN_SPREAD, _spawner.SPAWN_SPREAD)
		
	_spawn.position = pos
	
	var _velocity = _spawn.get_component("velocity") as Velocity
	
	var target = Vector2(240,240)
	target.x += (rand_range(-40, 40))
	target.y += (rand_range(-40, 40))
	
	_velocity.direction = (target - _spawn.global_position).normalized()
