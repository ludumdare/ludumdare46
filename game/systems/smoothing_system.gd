"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: SmoothingSystem
	
		Reduce Speed Depending on Distance From Target
	
	Remarks:

		As an Entity approaches it's Target we want to reduce its
		speed factor to give it the appearence that it is slowing
		down to move into postition.
		
		This will be used mostly with Shields when they are returning
		to their position around orbit.
		
"""


class_name SmoothingSystem
extends System

func on_process_entity(entity: Entity, delta: float):
	
	var _moveto  = entity.get_component("moveto") as MoveTo
	var _velocity = entity.get_component("velocity") as Velocity
	
	if (!_moveto.active):
		return
		
	if (!_moveto.target):
		return
		
	_velocity.SPEED_FACTOR = 15.0
	
	var _distance = entity.global_position.distance_to(_moveto.target.global_position)

	if _distance < 25:
		_velocity.SPEED_FACTOR = 10.0

	if _distance < 10:
		_velocity.SPEED_FACTOR = 5.0

	if _distance < 5:
		_velocity.SPEED_FACTOR = 2.5

	if _distance < 3:
		_velocity.SPEED_FACTOR = 0.5

	if _distance < 1:
		_velocity.SPEED_FACTOR = 0.0
		_moveto.active = false
		entity.get_parent().remove_child(entity)
		_moveto.target.add_child(entity)
		entity.position = Vector2.ZERO

