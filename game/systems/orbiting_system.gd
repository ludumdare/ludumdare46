"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: OrbitSystem
	
		A System for Entities that Orbit
	
	Remarks:
		
"""

class_name OrbitingSystem
extends System

func on_process_entity(entity: Entity, delta: float):
	
	var _orbits = entity.get_component("orbits") as Orbits
	
