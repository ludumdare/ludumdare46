class_name RotatingSystem
extends System

func on_process_entity(entity : Entity, delta):
	
	var _rotates = entity.get_component("rotates") as Rotates
	entity.rotation += _rotates.ROTATION_DIRECTION * _rotates.ROTATION_SPEED * _rotates.ROTATION_SPEED_FACTOR * delta
	if (entity.rotation < 0):
		entity.rotation = 360
