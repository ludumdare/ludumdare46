extends System
class_name MovementSystem

func on_process(entities, delta):

	for entity in entities:

		var _velocity = entity.get_component("velocity") as Velocity

		_velocity.velocity += _velocity.direction * _velocity.SPEED * _velocity.SPEED_FACTOR
		entity.position += _velocity.velocity * delta
		_velocity.velocity = Vector2.ZERO
