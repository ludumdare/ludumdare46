class_name ShieldHitSystem
extends System

func on_process_entity(entity: Entity, delta: float):
	
	var _collides = entity.get_component("collides") as Collides
	
	if (!_collides.has_collided):
		return
		
	print(_collides.entity.name)
	
	
	if (_collides.entity.name.to_lower().find("small") >= 0):
		return
		
	Logger.info("boom goes the shield")
	entity.remove_component("collides")
	ECS.remove_entity(entity)
	
	Game.play_zap()
	Game.current_player.shields -= 1

	var p = Game.particle_shield_hit.instance()
	entity.get_parent().add_child(p)
	p.global_position = entity.global_position
	Logger.info("p: {0}; entity: {1}".format([p.global_position, entity.global_position]))
