extends System

func on_process_entity(entity : Entity, delta):
	
	var _moveto  = entity.get_component("moveto") as MoveTo
	var _velocity = entity.get_component("velocity") as Velocity
	
	if (!_moveto.target):
		return
		
	if (!_moveto.active):
		return
	
	_velocity.direction = (_moveto.target.global_position - entity.global_position).normalized()
	
	
