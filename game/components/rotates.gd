class_name Rotates
extends Component

enum RotationDirections {
	DIRECTION_FORWARD = 1,
	DIRECTION_REVERSE = -1,
	DIRECTION_NONE = 0
}

export var ROTATION_SPEED : int = 1
export var ROTATION_SPEED_FACTOR : float = 1.0
export(RotationDirections) var ROTATION_DIRECTION : int = 1
