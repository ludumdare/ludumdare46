"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: Colides
	
		A Component for Entities that Collide
	
	Remarks:

		Attach this component to any Entity that
		you wish to collide with another
		Entity.
		
		Upon collision, the has_collided property
		will be set to True, indicating that it
		has collided with something.
		
		The first Entity it has collided with will
		be stored in the entity property for the
		developer to use.
		
"""

class_name Collides
extends Component

var has_collided : bool
var entity : Entity
var active : bool = true
