"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: MoveTo
	
		A Component for Move an Entity To a Target
	
	Remarks:

		Attach this Component to any Entity that you
		want to move towards another Target.
		
"""

class_name MoveTo
extends Component

var target : Entity = null
var active : bool = true
