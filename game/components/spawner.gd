class_name Spawner
extends Component

enum SpawnLocation {
	SPAWN_LOCATION_TOP = 0,
	SPAWN_LOCATION_BOTTOM = 1,
	SPAWN_LOCATION_LEFT = 2,
	SPAWN_LOCATION_RIGHT = 3
}

var spawn : bool

export var SPAWN_DELAY = 1.0
export var SPAWN_LOCATION = SpawnLocation.SPAWN_LOCATION_LEFT
export var SPAWN_SPREAD = 50
export var SPAWN_COUNT = 10
export(PackedScene) var SPAWN_SCENE
