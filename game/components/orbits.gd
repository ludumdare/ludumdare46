"""
	Copyright 2020 SpockerDotNet LLC
	
	Class: Orbits
	
		A Component for Entities that will Orbit
	
	Remarks:
		
"""

class_name Orbits
extends Component

var orbit_entity : Entity = null
var orbit_position : int = 0
