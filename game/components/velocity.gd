extends Component
class_name Velocity

export var SPEED: int = 10
export var SPEED_FACTOR: float = 1.5

var direction: Vector2 = Vector2.ZERO
var velocity: Vector2 = Vector2.ZERO

