extends Node2D

onready var tween = $Tween
onready var sprite = $Sprite

export var PUMP_MAX = 2.0
export var PUMP_SPEED = 1.0

func _ready():
	tween.interpolate_property(sprite, 'scale',
		sprite.scale, Vector2(PUMP_MAX, PUMP_MAX), PUMP_SPEED,
		Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.repeat = true
	tween.start()

func change_speed(speed):
	tween.playback_speed = speed
