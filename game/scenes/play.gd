class_name Play
extends Node2D

enum PlayState {
	PLAY_STATE_NONE,
	PLAY_STATE_INIT,
	PLAY_STATE_START,
	PLAY_STATE_WIN,
	PLAY_STATE_LOSE,
	PLAY_STATE_RUNNING,
	PLAY_STATE_FINISH
}

var play_state
var next_state
var job : JobInformation
var waves = []
var bag = []


var wave_total
var wave_asteroids
var wave_aliens
var wave_monsters
var wave_damage

var current_wave : int


func change_state(state):
	Logger.trace("[Game] change_state")
	next_state = state
	

func _on_wave_damage(value):
	wave_damage += value
	$ShakeCamera.add_trauma(0.5)
	
	if (wave_damage > job.strength):
		Game.current_player.currency -= 10
		if (Game.current_player.currency < 0):
			Game.current_player.currency = 0
		change_state(PlayState.PLAY_STATE_LOSE)
	
	
func _on_add_score(value):
	Game.current_player.score += value
	

func _on_add_currency(value):
	Game.current_player.currency += value
	
	
func _on_wave_completed():
	Game.current_player.last_earning = job.value
	Game.current_player.currency += job.value
	change_state(PlayState.PLAY_STATE_FINISH)
	

func _on_wave_item_destroyed(key : String):
	Logger.trace("[Play] _on_wave_item_destroyed")
	Logger.debug("- key : {0}".format([key]))
	
	if (key.find("asteroid") > 0):
		wave_asteroids -= 1
		
	wave_total -= 1
	
	if (wave_total < 1):
		Game.emit_signal("wave_completed")
	
func _process(delta):
	
	ECS.update("common")
	ECS.update("ui")
	
	match play_state:
		
		PlayState.PLAY_STATE_INIT:
			_play_state_init()
			
		PlayState.PLAY_STATE_START:
			_play_state_start()
			
		PlayState.PLAY_STATE_FINISH:
			_play_state_finish()
			
		PlayState.PLAY_STATE_LOSE:
			_play_state_lose()
			
		PlayState.PLAY_STATE_RUNNING:
			_play_state_running()
			

	ECS.update("debug")
	
#	var debug = "score : {0}\nwave_total : {1}\nwave_asteroids : {2}\nwave_damage : {3}/{4}\n".format([0, wave_total, wave_asteroids, wave_damage, job.strength])
#	$UI/Control/Debug.text = debug
	
	# update ui elements (hack)
	
	$"UI/Control/HealthPanel/#Health".text = str(job.strength - wave_damage)
	$"UI/Control/ItemPanel/#Shields".text = str(Game.current_player.shields)
	$"UI/Control/ItemPanel/#Satellites".text = str(Game.current_player.satellites)
	$"UI/Control/ItemPanel/#Rockets".text = str(Game.current_player.rockets)

	$"UI/Control/WavePanel/#Wave".text = "Wave 1/1"
	$"UI/Control/WavePanel/#Asteroids".text = str(wave_total)
	$"UI/Control/WavePanel/#Aliens".text = "0"
	$"UI/Control/WavePanel/#Monsters".text = "0"
					
	play_state = next_state
	
	
func _play_state_init():
	Logger.trace("[Play] _play_state_init")
	Logger.info(Game.current_player.name)
	change_state(PlayState.PLAY_STATE_START)

	
func _play_state_running():
#	Logger.trace("[Play] _play_state_running")
	ECS.update("running")


func _play_state_start():
	Logger.trace("[Play] _play_state_start")
	$Controllers/ShieldOrbitController.ORBIT_NODES = Game.current_player.shields
	$Controllers/ShieldOrbitController.create_orbit()
	$Controllers/ShipOrbitController.create_orbit()
	$Controllers/SatelliteOrbitController.ORBIT_NODES = Game.current_player.satellites
	$Controllers/SatelliteOrbitController.create_orbit()
	$Controllers/WaveController.init(waves[0])
	change_state(PlayState.PLAY_STATE_RUNNING)


func _play_state_finish():
	Logger.trace("[Play] _play_state_finish")
	ECS.clean()
	change_state(PlayState.PLAY_STATE_NONE)
	Game.change_state(Game.GameState.GAME_STATE_WIN)

func _play_state_win():
	Logger.trace("[Play] _play_state_win")


func _play_state_lose():
	Logger.trace("[Play] _play_state_lose")
	$Controllers/WaveController.active = false
	ECS.clean()
	change_state(PlayState.PLAY_STATE_NONE)
	Game.change_state(Game.GameState.GAME_STATE_LOSE)


func _ready():
	Logger.trace("[Play] _ready")
	
	Game.connect("wave_completed", self, "_on_wave_completed")
	Game.connect("wave_item_destroyed", self, "_on_wave_item_destroyed")
	Game.connect("wave_damage", self, "_on_wave_damage")
	Game.connect("add_currency", self, "_on_add_currency")
	
	change_state(PlayState.PLAY_STATE_INIT)
	

func _add_to_bag(key : String, amount : int):
	for i in range(amount):
		bag.append(key)
	
	
func _create_wave():
	var _wave : WaveData = WaveData.new()
	var _items = []
		
	var _bag = bag
	
	for i in range(bag.size()):
		
		var x = floor(rand_range(0, _bag.size()))
		var _item : WaveItem = WaveItem.new()
		_item.key = _bag[x]
		_item.direction = _get_direction()
		_items.append(_item)
		_bag.remove(x)
	
	
	_wave.name = "1"
	_wave.items = _items
	
	return _wave
		
func _get_direction():
	var _dirs = ["LEFT", "RIGHT", "UP", "DOWN"]
	return _dirs[rand_range(0, _dirs.size())]
	
	
func _init():
	Logger.trace("[Play] _init")
	
	job = JobInformation.new()
	job.name = "Earth 2"
	job.value = 30
	job.asteroid0 = 20
	job.asteroid1 = 5
	job.asteroid2 = 0
	job.alien0 = 0
	job.monster_count = 0
	job.monster_strength = 0
	job.strength = 8
	job.fine = 1
	
	var _wave : WaveData
	
	_add_to_bag("asteroid0", job.asteroid0)
	_add_to_bag("asteroid1", job.asteroid1)
	_add_to_bag("asteroid2", job.asteroid2)
	_add_to_bag("alien0", job.alien0)
	_wave = _create_wave()
	
	waves.append(_wave)
	
	wave_asteroids = job.asteroid0 + job.asteroid1 + job.asteroid2
	wave_aliens = job.alien0 
	wave_monsters = job.monster_count
	wave_total = wave_asteroids + wave_aliens + wave_monsters
	wave_damage = 0
	
