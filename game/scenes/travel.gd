class_name TravelScene
extends Node2D

enum TravelStates {
	TRAVEL_STATE_NONE,
	TRAVEL_STATE_TITLE,
	TRAVEL_STATE_ABOUT,
	TRAVEL_STATE_START,
	TRAVEL_STATE_END,
	TRAVEL_STATE_WIN,
	TRAVEL_STATE_LOSE,
	TRAVEL_STATE_SHOP,
	TRAVEL_STATE_UPGRADE,
	TRAVEL_STATE_JOB,
	TRAVEL_STATE_HOME,
	TRAVEL_STATE_PLAY
}

var state
var next_state


func change_state(state):
	next_state = state
	
	
func _process(delta):
	
	ECS.update()
	
	match state:
		
		TravelStates.TRAVEL_STATE_NONE:
			_hide_panels()
			
		TravelStates.TRAVEL_STATE_START:
			_hide_panels()
			
		TravelStates.TRAVEL_STATE_TITLE:
			_hide_panels()
			$UI/TITLE.visible = true
			
		TravelStates.TRAVEL_STATE_ABOUT:
			_hide_panels()
			$UI/ABOUT.visible = true
			
		TravelStates.TRAVEL_STATE_JOB:
			_hide_panels()
			$UI/JOB.visible = true
			$UI/OPTIONS.visible = true
			
		TravelStates.TRAVEL_STATE_SHOP:
			_hide_panels()
			$UI/SHOP.visible = true
			$UI/READY.visible = true
			
		TravelStates.TRAVEL_STATE_LOSE:
			_hide_panels()
			$UI/OPTIONS.visible = true
			$UI/LOSE.visible = true
			
		TravelStates.TRAVEL_STATE_WIN:
			_hide_panels()
			$UI/OPTIONS.visible = true
			$UI/WIN.visible = true
			
		TravelStates.TRAVEL_STATE_PLAY:
			_hide_panels()
			Game.change_state(Game.GameState.GAME_STATE_PLAY)
			change_state(TravelStates.TRAVEL_STATE_NONE)
			
		TravelStates.TRAVEL_STATE_HOME:
			_hide_panels()
			Game.change_state(Game.GameState.GAME_STATE_HOME)
			change_state(TravelStates.TRAVEL_STATE_NONE)
			
	
	state = next_state


func _refresh_data():
	
	$"UI/Funds/#Currency".text = str(Game.current_player.currency)
	
	# buying menu
	$"UI/SHOP/Panel/VBoxContainer/BuyContainer/BuyShields/BuyShieldsContainer/TextureRect/#Shields".text = str(Game.current_player.shields)
	$"UI/SHOP/Panel/VBoxContainer/BuyContainer/BuyShields/BuyShieldsContainer/#BuyShieldCost".text = str(Game.data.shops[0].cost)
	$"UI/SHOP/Panel/VBoxContainer/BuyContainer/BuySatellites/BuySatellitesContainer/TextureRect/#Satellites".text = str(Game.current_player.satellites)
	$"UI/SHOP/Panel/VBoxContainer/BuyContainer/BuySatellites/BuySatellitesContainer/#BuySatelliteCost".text = str(Game.data.shops[1].cost)
	$"UI/SHOP/Panel/VBoxContainer/BuyContainer/BuyRockets/BuyRocketsContainer/#BuyRocketCost".text = str(Game.data.shops[2].cost)
	$"UI/SHOP/Panel/VBoxContainer/BuyContainer/BuyRockets/BuyRocketsContainer/TextureRect/#Rockets".text = str(Game.current_player.rockets)
	
	$"UI/WIN/#CurrencyEarned".text = str(Game.current_player.last_earning)
	
func _ready():
	change_state(Game.travel_state)
	_refresh_data()

func _init():
	change_state(TravelStates.TRAVEL_STATE_NONE)

func _on_Play_pressed():
	$Spawners/StarSpawner.active = false
	$Spawners/CometSpawner.active = false
	ECS.clean()
	Game.change_state(Game.GameState.GAME_STATE_SHOP)
	change_state(TravelStates.TRAVEL_STATE_NONE)


func _hide_panels():
	$UI/OPTIONS.visible = false
	$UI/JOB.visible = false
	$UI/SHOP.visible = false
	$UI/WIN.visible = false
	$UI/LOSE.visible = false
	$UI/TITLE.visible = false
	$UI/READY.visible = false
	$UI/ABOUT.visible = false
	
func _on_Home_pressed():
	$Spawners/StarSpawner.active = false
	$Spawners/CometSpawner.active = false
	ECS.clean()
	change_state(TravelStates.TRAVEL_STATE_HOME)


func _on_EasyButton_pressed():
	$UI/JOB/HBoxContainer/EasyPanel/EasyContainer/EasySelected.visible = true
	$UI/JOB/HBoxContainer/NormalPanel/NormalContainer/NormalSelected.visible = false
	$UI/JOB/HBoxContainer/HardPanel/HardContainer/HardSelected.visible = false

func _on_NormalButton_pressed():
	$UI/JOB/HBoxContainer/NormalPanel/NormalContainer/NormalSelected.visible = true
	$UI/JOB/HBoxContainer/EasyPanel/EasyContainer/EasySelected.visible = false
	$UI/JOB/HBoxContainer/HardPanel/HardContainer/HardSelected.visible = false


func _on_HardButton_pressed():
	$UI/JOB/HBoxContainer/HardPanel/HardContainer/HardSelected.visible = true
	$UI/JOB/HBoxContainer/EasyPanel/EasyContainer/EasySelected.visible = false
	$UI/JOB/HBoxContainer/NormalPanel/NormalContainer/NormalSelected.visible = false


func _on_BuyShields_pressed():
	
	if (Game.current_player.currency >= Game.data.shops[0].cost):
			Game.current_player.currency -= Game.data.shops[0].cost
			Game.current_player.shields += 2
			
	_refresh_data()


func _on_BuySatellites_pressed():
	
	if (Game.current_player.currency >= Game.data.shops[1].cost):
		Game.current_player.currency -= Game.data.shops[1].cost
		Game.current_player.satellites += 1
			
	_refresh_data()


func _on_BuyRockets_pressed():
	
	if (Game.current_player.currency >= Game.data.shops[2].cost):
		Game.current_player.currency -= Game.data.shops[2].cost
		Game.current_player.rockets += 2
			
	_refresh_data()


func _on_TitlePlay_pressed():
	$Spawners/StarSpawner.active = false
	$Spawners/CometSpawner.active = false
	ECS.clean()
	Game.change_state(Game.GameState.GAME_STATE_JOB)
	change_state(TravelStates.TRAVEL_STATE_NONE)


func _on_ReadyButton_pressed():
	$Spawners/StarSpawner.active = false
	$Spawners/CometSpawner.active = false
	ECS.clean()
	Game.change_state(Game.GameState.GAME_STATE_PLAY)
	change_state(TravelStates.TRAVEL_STATE_NONE)


func _on_TitleQuit_pressed():
	get_tree().quit()


func _on_TitleAbout_pressed():
	$Spawners/StarSpawner.active = false
	$Spawners/CometSpawner.active = false
	ECS.clean()
	Game.change_state(Game.GameState.GAME_STATE_ABOUT)
	change_state(TravelStates.TRAVEL_STATE_NONE)
